import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { position } from "../types/backendTypes/entities";

export const PostionContainer = ({
  children,
  log = false,
  ghostRefreshToken,
}: {
  children: any;
  log?: boolean;
  ghostRefreshToken?: number;
}) => {
  const {
    simpleQuery: personnelQuery,
    Query,
    refreshGET,
  } = useContainer("organisation/position", {
    log: false,
    keys: ["position", ghostRefreshToken],
    defaultValue: [],
    case200(data: position[]) {
      log &&
        console.log("container organisation/personnel case200 data:", data);
      return data;
    },
  });
  return <>{ChildrenWithProps(children, personnelQuery, Query, refreshGET)}</>;
};
