import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { jobholder } from "../types/backendTypes/entities";

export const JobholderContainer = ({
  children,
  log = false,
  ghostRefreshToken,
}: {
  children: any;
  log?: boolean;
  ghostRefreshToken?: number;
}) => {
  const {
    simpleQuery: personnelQuery,
    Query,
    refreshGET,
  } = useContainer("organisation/jobholder", {
    log: false,
    keys: ["position", ghostRefreshToken],
    defaultValue: [],
    case200(data: jobholder[]) {
      log && console.log("organisation/jobholder case200 data:", data);
      return data;
    },
  });
  return <>{ChildrenWithProps(children, personnelQuery, Query, refreshGET)}</>;
};
