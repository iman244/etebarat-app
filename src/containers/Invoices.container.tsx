import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";

export const InvoicesContainer = ({
  children,
  log = false,
}: {
  children: any;
  log?: boolean;
}) => {
  const {
    simpleQuery: invoicesQuery,
    Query,
    refreshGET,
  } = useContainer("credit/invoices", {
    log: false,
    keys: ["invoices"],
    defaultValue: [],
  });

  log &&
    console.log("InvoicesContainer credit/invoice simpleQuery", invoicesQuery);
  log && console.log("InvoicesContainer credit/invoice Query", Query);
  return ChildrenWithProps(children, invoicesQuery, Query, refreshGET);
};
