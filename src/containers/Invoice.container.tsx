import React from "react";
import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { useFormContext } from "react-hook-form";

export const InvoiceContainer = ({
  children,
  log = false,
}: {
  children: any;
  log?: boolean;
}) => {
  const { watch } = useFormContext();
  const {
    simpleQuery: invoiceQuery,
    Query,
    refreshGET,
  } = useContainer(
    `credit/invoices/${watch("invoiceId") && watch("invoiceId").value}`,
    {
      enabled: !!watch("invoiceId"),
      log: false,
      keys: watch("invoiceId")
        ? ["invoice", watch("invoiceId").value]
        : ["invoice"],
      defaultValue: [],
    }
  );

  log &&
    console.log("InvoiceContainer credit/invoice simpleQuery", invoiceQuery);
  log && console.log("InvoiceContainer credit/invoice Query", Query);
  return ChildrenWithProps(children, invoiceQuery, Query, refreshGET);
};
