import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { job, permisionLetter } from "../types/backendTypes/entities";

export const JobsContainer = ({
  children,
  log = false,
}: {
  children: any;
  log?: boolean;
}) => {
  const {
    simpleQuery: jobsQuery,
    Query,
    refreshGET,
  } = useContainer("credit/job", {
    log: false,
    keys: ["job"],
    defaultValue: [],
    case200(data: job[]) {
      log && console.log("container job case200 data:", data);
      return data.map((job) => {
        return {
          ...job,
          molahezatCode: digitsEnToFa(job.molahezatCode),
          description: digitsEnToFa(job.description),
          creditValue: digitsEnToFa(job.creditValue),
          permisionLetter: {
            ...job.permisionLetter,
            budgetCode: digitsEnToFa(job.permisionLetter.budgetCode),
            activityCode: digitsEnToFa(job.permisionLetter.activityCode),
            date: digitsEnToFa(job.permisionLetter.date),
            letterNo: digitsEnToFa(job.permisionLetter.letterNo),
          },
        };
      });
    },
  });
  return <>{ChildrenWithProps(children, jobsQuery, Query, refreshGET)}</>;
};
