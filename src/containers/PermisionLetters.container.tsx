import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { permisionLetter } from "../types/backendTypes/entities";

export const PermisionLettersContainer = ({
  children,
  log = false,
}: {
  children: any;
  log?: boolean;
}) => {
  const {
    simpleQuery: perLettersQuery,
    Query,
    refreshGET,
  } = useContainer("credit/permision-letters", {
    log: false,
    keys: ["permision-letters"],
    defaultValue: [],
    case200(data: permisionLetter[]): permisionLetter[] {
      log && console.log("container permision-letters case200 data:", data);
      return data.map((permisionLetter) => {
        return {
          permisionLetterId: permisionLetter.permisionLetterId,
          letterNo: digitsEnToFa(permisionLetter.letterNo),
          date: digitsEnToFa(permisionLetter.date),
          budgetCode: digitsEnToFa(permisionLetter.budgetCode),
          activityCode: digitsEnToFa(permisionLetter.activityCode),
          jobs: permisionLetter.jobs,
        };
      });
    },
  });
  return <>{ChildrenWithProps(children, perLettersQuery, Query, refreshGET)}</>;
};
