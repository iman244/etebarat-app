import { ChildrenWithProps, useContainer } from "../hooks/useContainer";
import { digitsEnToFa } from "@persian-tools/persian-tools";
import { person } from "../types/backendTypes/entities";

export const PersonnelContainer = ({
  children,
  log = false,
  ghostRefreshToken,
}: {
  children: any;
  log?: boolean;
  ghostRefreshToken?: number;
}) => {
  const {
    simpleQuery: personnelQuery,
    Query,
    refreshGET,
  } = useContainer("organisation/personnel", {
    log: false,
    keys: ["personnel", ghostRefreshToken],
    defaultValue: [],
    case200(data: person[]) {
      log &&
        console.log("container organisation/personnel case200 data:", data);
      let perConverted = data.map((person) => {
        return {
          ...person,
          personnelId: digitsEnToFa(person.personnelId),
        };
      });
      return perConverted;
    },
  });

  log && console.log("PersonnelContainer refreshGET", refreshGET);
  return <>{ChildrenWithProps(children, personnelQuery, Query, refreshGET)}</>;
};
