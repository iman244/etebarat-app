import React, { useEffect } from "react";
import { ConnectForm } from "../Form";
import { ComplicatedInput, ComplicatedSelectInput } from "../FormComponents";
import { InputGroup, InputLeftElement, Text } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";
import {
  addCommas,
  digitsEnToFa,
  numberToWords,
} from "@persian-tools/persian-tools";
import { useContainer } from "../../hooks/useContainer";
import { jobholder } from "../../types/backendTypes/entities";
import { StylesConfig } from "react-select";

export const DescriptionInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"description"}
          placeholder={"شرح"}
          rules={{
            required: {
              value: true,
              message: "شرح اعتبار را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          type={"textarea"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const DetailedCodeInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"molahezatCode"}
          placeholder={"کد ملاحظات"}
          rules={{
            required: {
              value: true,
              message: "کد ملاحظات را وارد کنید",
            },
            pattern: {
              value: /^[0-9]{4}$/g,
              message: "کد ملاحظات صحیح نیست",
            },
          }}
          type={"number"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const OperationCodeInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"operationode"}
          placeholder={"برنامه"}
          rules={{
            required: {
              value: true,
              message: "شماره برنامه را وارد کنید",
            },
            pattern: {
              value: /^[0-9]{1}$/g,
              message: "شماره برنامه صحیح نیست",
            },
          }}
          type={"number"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const CreditInput = ({
  placeholder = "مبلغ",
  w = "216px",
}: {
  placeholder?: string;
  w?: string;
}) => {
  const { watch } = useFormContext();

  return (
    <ConnectForm>
      {({ register }: any) => (
        <>
          <InputGroup size="md">
            <ComplicatedInput
              register={register}
              name={"creditValue"}
              placeholder={placeholder}
              rules={{
                required: {
                  value: true,
                  message: "مبلغ را وارد کنید",
                },
                pattern: {
                  value: /^[0-9]+/g,
                  message: "مبلغ صحیح نیست",
                },
              }}
              type={"number"}
              w={w}
            />
            <InputLeftElement mt={"28px"}>ریال</InputLeftElement>
          </InputGroup>
          <Text mt={"8px"} fontSize={"0.75em"} w={w}>
            {watch("creditValue") &&
              digitsEnToFa(addCommas(watch("creditValue").toString()))}{" "}
            {watch("creditValue") && "ریال"}
          </Text>
          <Text mt={"8px"} fontSize={"0.75em"} w={w}>
            {watch("creditValue") &&
              numberToWords(watch("creditValue")).toString()}{" "}
            {watch("creditValue") && "ریال"}
          </Text>
        </>
      )}
    </ConnectForm>
  );
};

export const SelectJobholderInput = ({
  log = false,
  personnelId,
}: {
  log?: boolean;
  personnelId?: number;
}) => {
  const {
    simpleQuery: jobholderQuery,
    Query,
    refreshGET,
  } = useContainer("organisation/jobholder", {
    log: false,
    keys: ["jobholder"],
    defaultValue: [],
  });

  log && console.log("SelectJobholderInput personnelId", personnelId);
  log &&
    console.log(
      "SelectJobholderInput organisation/jobholder simpleQuery",
      jobholderQuery
    );
  log &&
    console.log("SelectJobholderInput organisation/jobholder Query", Query);

  // we show just jobholder that have been assigned a person
  const options: any = jobholderQuery.data
    .filter((jobholder: jobholder) => {
      if (jobholder.person) {
        return true;
      }
    })
    .map((jobholder: jobholder) => ({
      label: `افسر خرید ${jobholder.positionName} - ${jobholder.person?.lastName}`,
      value: jobholder.person?.personnelId,
    }));

  const defaultValue: { label: string; value: number }[] = options.filter(
    (option: any) => option.value === personnelId
  );

  const { handleSubmit, control, setValue } = useFormContext();

  useEffect(() => {
    if (personnelId && defaultValue.length) {
      setValue("personnelId", {
        label: defaultValue[0].label,
        value: defaultValue[0].value,
      });
    }
  }, [defaultValue]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"personnelId"}
          register={register}
          placeholder="انتخاب نیرو"
          rules={{
            required: {
              value: true,
              message: "پرسنل مورد نظر را انتخاب کنید",
            },
          }}
          options={options}
          selectStyles={SelectJobholderStyles}
          defaultValue={options[0]}
          w={"100%"}
        />
      )}
    </ConnectForm>
  );
};

export const SelectJobholderStyles: StylesConfig<any> = {
  control: (styles) => ({ ...styles, fontSize: "0.7rem" }),
};
