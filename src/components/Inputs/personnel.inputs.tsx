import { ConnectForm } from "../Form";
import { ComplicatedInput } from "../FormComponents";

export const NameInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"name"}
          placeholder={"نام"}
          rules={{
            required: {
              value: true,
              message: "نام را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const LastNameInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"lastName"}
          placeholder={"نام خانوادگی"}
          rules={{
            required: {
              value: true,
              message: "نام خانوادگی را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const PersonnelIdInput = ({
  w = "216px",
  value,
}: {
  w?: string;
  value?: number;
}) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"personnelId"}
          placeholder={"شماره پرسنلی"}
          rules={{
            required: {
              value: true,
              message: "شماره پرسنلی را وارد کنید",
            },
          }}
          w={w}
          fontSize={"0.875em"}
          type="number"
        />
      )}
    </ConnectForm>
  );
};

export const RankInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"rank"}
          placeholder={"درجه"}
          rules={{
            required: {
              value: true,
              message: "درجه را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};
