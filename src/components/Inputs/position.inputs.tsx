import { useEffect } from "react";
import { ConnectForm } from "../Form";
import { ComplicatedInput, ComplicatedSelectInput } from "../FormComponents";
import { StylesConfig } from "react-select";
import { useFormContext } from "react-hook-form";
import { person } from "../../types/backendTypes/entities";
import { useContainer } from "../../hooks/useContainer";

export const PositionNameInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"positionName"}
          placeholder={"نام موقعیت"}
          rules={{
            required: {
              value: true,
              message: "نام موقعیت را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const SelectPersonInput = ({
  log = false,
  personnelId,
}: {
  log?: boolean;
  personnelId?: number;
}) => {
  const {
    simpleQuery: personnelQuery,
    Query,
    refreshGET,
  } = useContainer("organisation/personnel", {
    log: false,
    keys: ["personnel"],
    defaultValue: [],
  });

  const options: any = personnelQuery.data.map((person: person) => ({
    label: `${person.rank} ${person.name} ${person.lastName}`,
    value: person.personnelId,
  }));

  const defaultValue: { label: string; value: number }[] = options.filter(
    (option: any) => option.value === personnelId
  );

  const { handleSubmit, control, setValue } = useFormContext();

  useEffect(() => {
    if (personnelId && defaultValue.length) {
      setValue("personnelId", {
        label: defaultValue[0].label,
        value: defaultValue[0].value,
      });
    }
  }, [defaultValue]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"personnelId"}
          register={register}
          placeholder="انتخاب نیرو"
          rules={{
            required: {
              value: true,
              message: "پرسنل مورد نظر را انتخاب کنید",
            },
          }}
          options={options}
          selectStyles={SelectPersonStyles}
          defaultValue={options[0]}
        />
      )}
    </ConnectForm>
  );
};

const SelectPersonStyles: StylesConfig<any> = {
  control: (styles) => ({ ...styles, width: "200px", fontSize: "0.7rem" }),
};
