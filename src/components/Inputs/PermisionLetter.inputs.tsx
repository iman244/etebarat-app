import { useFormContext } from "react-hook-form";
import { selectOptions, simpleQuery } from "../../types/frontTypes";
import { ConnectForm } from "../Form";
import { ComplicatedInput, ComplicatedSelectInput } from "../FormComponents";
import { useEffect } from "react";

export const PermisionLetterNoSelectInput = () => {
  const options: selectOptions = [
    {
      label: "751/1/200",
      value: "751/1/200",
    },
    {
      label: "751/1/201",
      value: "751/1/201",
    },
    {
      label: "751/1/202",
      value: "751/1/202",
    },
  ];
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"letterNo"}
          rules={{
            required: {
              value: true,
              message: "نامه ایجاد تعهد را انتخاب کنید",
            },
          }}
          register={register}
          placeholder="انتخاب نامه ایجاد تعهد"
          options={options}
        />
      )}
    </ConnectForm>
  );
};

export const PermisionLetterNoTextInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"letterNo"}
          placeholder={"شماره نامه ایجاد تعهد"}
          rules={{
            required: {
              value: true,
              message: "شماره نامه ایجاد تعهد را وارد کنید",
            },
            pattern: {
              value: /^751\/[0-9]+\/[0-9]+$/g,
              message: "مثال: ###/#/751",
            },
          }}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const ActivityCodeInput = () => {
  const options: selectOptions = [
    {
      label: "1803001001 توضیح",
      value: "1803001001",
    },
    {
      label: "1101040007 توضیح",
      value: "1101040007",
    },
  ];
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"activityCode"}
          register={register}
          placeholder="شماره فعالیت"
          rules={{
            required: {
              value: true,
              message: "شماره فعالیت را کنید",
            },
          }}
          options={options}
        />
      )}
    </ConnectForm>
  );
};

export const BudgetCodeInput = () => {
  const options: selectOptions = [
    {
      label: "111215",
      value: "111215",
    },
    {
      label: "111200",
      value: "111200",
    },
    {
      label: "1112001",
      value: "1112001",
    },
  ];
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"budgetCode"}
          register={register}
          placeholder="کد بودجه"
          rules={{
            required: {
              value: true,
              message: "کد بودجه را انتخاب کنید",
            },
          }}
          options={options}
        />
      )}
    </ConnectForm>
  );
};

export const DateInput = ({
  log = false,
  simpleQuery = { data: undefined, status: "loading" },

  w = "216px",
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<{ date: string } | undefined>;
  w?: string;
}) => {
  const date = simpleQuery.data?.date;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (date !== undefined) {
      log && console.log("setValue(date)", date);
      setValue("date", date);
    }
  }, [date]);

  log && console.log("DateInput simpleQuery", simpleQuery);
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"date"}
          placeholder={"تاریخ"}
          rules={{
            required: {
              value: true,
              message: "تاریخ را وارد کنید",
            },
            pattern: {
              value: /14[0-9]{2}\/(0|1)[0-9]\/([0-3][0-9])$/g,
              message: "مثال: ##/##/##14",
            },
          }}
          w={w}
        />
      )}
    </ConnectForm>
  );
};
