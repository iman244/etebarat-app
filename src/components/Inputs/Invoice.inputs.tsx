import { StylesConfig } from "react-select";
import { useContainer } from "../../hooks/useContainer";
import { invoice, job } from "../../types/backendTypes/entities";
import { ConnectForm } from "../Form";
import {
  ComplicatedCreatableSelectInput,
  ComplicatedInput,
  ComplicatedSelectInput,
} from "../FormComponents";
import usePost from "../../hooks/usePost";
import { useEffect, useState } from "react";
import { query, simpleQuery } from "../../types/frontTypes";
import { useFormContext } from "react-hook-form";
import { Flex } from "@chakra-ui/react";
import {
  addCommas,
  digitsEnToFa,
  numberToWords,
} from "@persian-tools/persian-tools";
import { Field } from "../TextComponents";

export function jobLabel(job: job): string {
  return `${job.personnel.lastName} - ${job.permisionLetter.letterNo} - ${job.molahezatCode} - ${job.description}`;
}

export const SelectJobInput = ({
  simpleQuery = { data: undefined, status: "loading" },
  log = false,
}: {
  simpleQuery?: simpleQuery<{ job: job | null } | undefined>;
  log?: boolean;
}) => {
  const {
    simpleQuery: jobQuery,
    Query,
    refreshGET,
  } = useContainer("credit/job", {
    log: false,
    keys: ["job"],
    defaultValue: [],
  });

  log && console.log("SelectJobInput credit/job simpleQuery", jobQuery);
  log && console.log("SelectJobInput credit/job Query", Query);

  const options: any = jobQuery.data.map((job: job) => ({
    label: jobLabel(job),
    value: job.jobId,
  }));

  const job = simpleQuery.data?.job;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (job !== undefined && job !== null) {
      setValue("jobId", {
        label: jobLabel(job),
        value: job.jobId,
      });
    }
  }, [job]);

  useEffect(() => {
    if (watch("invoiceId") == null) {
      setValue("jobId", null);
    } else if (job !== undefined && job !== null) {
      setValue("jobId", {
        label: jobLabel(job),
        value: job.jobId,
      });
    }
  }, [watch("invoiceId")]);

  log && console.log(" SelectJobInput simpleQuery", simpleQuery);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedSelectInput
          name={"jobId"}
          register={register}
          placeholder="انتخاب اعتبار"
          rules={{
            required: {
              value: true,
              message: "اعتبار مورد نظر را انتخاب کنید",
            },
          }}
          options={options}
          selectStyles={SelectJobStyles}
          defaultValue={options[0]}
        />
      )}
    </ConnectForm>
  );
};

export const SelectInvoiceInput = ({
  simpleQuery = { data: [], status: "loading" },
  log = false,
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<invoice[]>;
}) => {
  log && console.log("SelectInvoiceInput simpleQuery", simpleQuery);
  const options: any = simpleQuery.data.map((invoice: invoice) => ({
    label: `${invoice.invoiceId} - ${
      invoice.job ? invoice.job.description : "محل اعتبار مشخص نشده است"
    }`,
    value: invoice.invoiceId,
  }));
  const loadingState = useState(false);
  let p = usePost("/credit/invoice", loadingState);

  const handleIssueInvoice = (newItem: any) => {
    console.log(newItem);
    // let a = { invoiceId: newItem };
    p.mutate({ invoiceId: newItem });
  };

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedCreatableSelectInput
          onCreateOption={handleIssueInvoice}
          rules={{
            required: {
              value: true,
              message: "فاکتور را ایجاد/انتخاب کنید",
            },
          }}
          name={"invoiceId"}
          register={register}
          placeholder="ایجاد/انتخاب فاکتور"
          options={options}
          selectStyles={SelectJobStyles}
          defaultValue={options[0]}
          w={"100%"}
        />
      )}
    </ConnectForm>
  );
};

export const SelectJobStyles: StylesConfig<any> = {
  control: (styles) => ({ ...styles, width: "300px", fontSize: "0.7rem" }),
  menu: (styles) => ({ ...styles, fontSize: "0.7rem" }),
};

export const VATInput = ({
  log = false,
  simpleQuery = { data: undefined, status: "loading" },
  placeholder = "مالیات بر ارزش افزوده",
  w = "216px",
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<invoice | undefined>;

  placeholder?: string;
  w?: string;
}) => {
  log && console.log("VATInput simpleQuery", simpleQuery);

  const vat = simpleQuery.data?.vat;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (vat !== undefined) {
      setValue("vat", vat);
    }
  }, [vat]);

  useEffect(() => {
    if (watch("invoiceId") === null) {
      setValue("vat", 0);
    } else if (vat !== undefined) {
      setValue("vat", vat);
    }
  }, [watch("invoiceId")]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <Flex gap={"12px"} flexDir={"column"}>
          <ComplicatedInput
            register={register}
            name={"vat"}
            placeholder={placeholder}
            rules={{
              required: {
                value: true,
                message: "مالیات بر ارزش افزوده را وارد کنید",
              },
              pattern: {
                value: /^[0-9]+/g,
                message: "مبلغ صحیح نیست",
              },
            }}
            type={"number"}
            w={w}
          />
          <Flex alignItems={"center"} fontSize={"xs"}>
            <Field
              field="مالیات بر ارزش افزوده"
              value={`${
                watch("vat")
                  ? digitsEnToFa(addCommas(watch("vat").toString()))
                  : "صفر"
              }
            ${"ریال"}`}
            />
          </Flex>
        </Flex>
      )}
    </ConnectForm>
  );
};

export const DiscountInput = ({
  log = false,
  simpleQuery = { data: undefined, status: "loading" },
  placeholder = "تخفیف",
  w = "216px",
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<invoice | undefined>;
  placeholder?: string;
  w?: string;
}) => {
  log && console.log("DiscountInput simpleQuery", simpleQuery);

  const discount = simpleQuery.data?.discount;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (discount !== undefined) {
      setValue("discount", discount);
    }
  }, [discount]);

  useEffect(() => {
    if (watch("invoiceId") === null) {
      setValue("discount", 0);
    } else if (discount !== undefined) {
      setValue("discount", discount);
    }
  }, [watch("invoiceId")]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <Flex gap={"12px"} flexDir={"column"}>
          <ComplicatedInput
            register={register}
            name={"discount"}
            placeholder={placeholder}
            rules={{
              required: {
                value: true,
                message: "تخفیف را وارد کنید",
              },
              pattern: {
                value: /^[0-9]+/g,
                message: "مبلغ صحیح نیست",
              },
            }}
            type={"number"}
            w={w}
          />
          <Flex alignItems={"center"} fontSize={"xs"}>
            <Field
              field="تخفیف"
              value={`${
                watch("discount")
                  ? digitsEnToFa(addCommas(watch("discount").toString()))
                  : "صفر"
              }
            ${"ریال"}`}
            />
          </Flex>
        </Flex>
      )}
    </ConnectForm>
  );
};

export const ForInput = ({
  log = false,
  simpleQuery = { data: undefined, status: "loading" },
  w = "216px",
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<invoice | undefined>;
  w?: string;
}) => {
  log && console.log("ForInput simpleQuery", simpleQuery);

  const For = simpleQuery.data?.for;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (For !== undefined) {
      setValue("for", For);
    }
  }, [For]);

  useEffect(() => {
    if (watch("invoiceId") === null) {
      setValue("for", "");
    } else if (For !== undefined) {
      setValue("for", For);
    }
  }, [watch("invoiceId")]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"for"}
          placeholder={"جهت"}
          rules={{
            required: {
              value: true,
              message: "جهت را وارد کنید",
            },
          }}
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const InvoiceDateInput = ({
  log = false,
  simpleQuery = { data: undefined, status: "loading" },

  w = "216px",
}: {
  log?: boolean;
  simpleQuery?: simpleQuery<{ date: string } | undefined>;
  w?: string;
}) => {
  log && console.log("InvoiceDateInput simpleQuery", simpleQuery);

  const date = simpleQuery.data?.date;
  const { handleSubmit, control, setValue, watch } = useFormContext();

  useEffect(() => {
    if (date !== undefined) {
      log && console.log("setValue(date)", date);
      setValue("date", date);
    }
  }, [date]);

  useEffect(() => {
    if (watch("invoiceId") === null) {
      setValue("date", "");
    } else if (date !== undefined) {
      setValue("date", date);
    }
  }, [watch("invoiceId")]);

  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          name={"date"}
          placeholder={"تاریخ"}
          rules={{
            required: {
              value: true,
              message: "تاریخ را وارد کنید",
            },
            pattern: {
              value: /14[0-9]{2}\/(0|1)[0-9]\/([0-3][0-9])$/g,
              message: "مثال: ##/##/##14",
            },
          }}
          w={w}
        />
      )}
    </ConnectForm>
  );
};
