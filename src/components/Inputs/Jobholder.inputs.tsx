import { ConnectForm } from "../Form";
import { ComplicatedInput } from "../FormComponents";

export const OpertaionNumberInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"operationNo"}
          placeholder={"شماره برنامه"}
          rules={{
            required: {
              value: true,
              message: "شماره برنامه را وارد کنید",
            },
          }}
          fontSize={"0.875em"}
          type="number"
          w={w}
        />
      )}
    </ConnectForm>
  );
};

export const JobholderPositionNameInput = ({ w = "216px" }: { w?: string }) => {
  return (
    <ConnectForm>
      {({ register }: any) => (
        <ComplicatedInput
          register={register}
          height={"100%"}
          name={"jobholderPositionName"}
          placeholder={"نام موقعیت افسر خرید"}
          rules={{
            required: {
              value: true,
              message: "نام موقعیت افسر خرید را وارد کنید (مثال: برنامه 1)",
            },
          }}
          fontSize={"0.875em"}
          w={w}
        />
      )}
    </ConnectForm>
  );
};
