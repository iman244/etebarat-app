import {
  TableContainer,
  Table as ChakraTable,
  Thead,
  Tr,
  Th,
  Tbody,
} from "@chakra-ui/react";
import React, { Fragment } from "react";

export const Table = ({ items }: any) => {
  const columns = [
    {
      head: "head",
      field: "activityCode",
    },
  ];
  return (
    <TableContainer>
      <ChakraTable variant={"striped"}>
        <Thead>
          <Tr>
            <Th>شماره فعالیت</Th>
            <Th>کد بودجه</Th>
            <Th>تاریخ</Th>
            <Th>شماره نامه</Th>
          </Tr>
        </Thead>
        <Tbody>
          {/* {items.data.map((item, index) => (
                  <Fragment key={index}>
                    <Tr
                      _hover={{
                        opacity: "0.5",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setIndexModalItem(index);
                        onOpen();
                      }}
                    >
                      <Td>{permisionLetter.activityCode}</Td>
                      <Td>{permisionLetter.budgetCode}</Td>
                      <Td>{permisionLetter.date}</Td>
                      <Td>{permisionLetter.letterNo}</Td>
                    </Tr>
                  </Fragment>
                ))} */}
        </Tbody>
      </ChakraTable>
    </TableContainer>
  );
};
