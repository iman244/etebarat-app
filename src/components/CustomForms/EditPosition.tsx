import { useState } from "react";
import Form from "../Form";
import { Flex, Text } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import { SelectPersonInput } from "../Inputs/position.inputs";
import { position } from "../../types/backendTypes/entities";
import { editPositionDto } from "../../types/backendTypes/dtos";

export const EditPosition = ({
  url,
  deleteUrl,
  position,
  refreshGET,
  log = false,
}: {
  url: string;
  deleteUrl?: string;
  position: position;
  refreshGET?: () => void;
  log?: boolean;
}) => {
  const { person } = position;

  log && console.log("EditPosition position", position);
  const loadingStatePatch = useState(false);
  const loadingStateDelete = useState(false);
  return (
    <Flex
      alignItems={"center"}
      gap={"12px"}
      justifyContent={"space-between"}
      w={"100%"}
    >
      <Form
        url={url}
        method="patch"
        loadingState={loadingStatePatch}
        w={"100%"}
        onSubmitF={(data: {
          personnelId: { label: string; value: number };
        }): editPositionDto => {
          log && console.log("organisation/position patch data", data);
          return {
            positionName: position.positionName,
            personnelId: data.personnelId?.value,
          };
        }}
        onSettledF={refreshGET && refreshGET}
      >
        <Flex
          gap={"24px"}
          alignItems={"center"}
          w={"100%"}
          justifyContent={"space-between"}
        >
          <Text>{position.positionName}</Text>

          <Flex gap={"12px"}>
            <SelectPersonInput personnelId={position.person?.personnelId} />
            <SubmitButton isloading={loadingStatePatch[0]}>انتصاب</SubmitButton>
          </Flex>
        </Flex>
      </Form>
      <Form
        url={deleteUrl ? deleteUrl : url}
        method="delete"
        loadingState={loadingStateDelete}
        w={"600px"}
        case200={refreshGET && refreshGET}
        onSubmitF={(): position => {
          return {
            positionName: position.positionName,
          };
        }}
      >
        <SubmitButton variant={"delete-btn"} isloading={loadingStateDelete[0]}>
          حذف موقعیت
        </SubmitButton>
      </Form>
    </Flex>
  );
};
