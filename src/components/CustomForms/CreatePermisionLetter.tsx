import { useState } from "react";
import Form from "../Form";
import { rawFormCredit } from "../../types/frontTypes";
import { Box, Grid, Heading } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import { CreatePermisionLetterDto } from "../../types/backendTypes/dtos";
import {
  ActivityCodeInput,
  BudgetCodeInput,
  DateInput,
  PermisionLetterNoTextInput,
} from "../Inputs/PermisionLetter.inputs";

export const CreatePermisionLetter = ({
  refreshGET,
}: {
  refreshGET?: () => void;
}) => {
  const CL_loadingState = useState(false);

  return (
    <Form
      url="/credit/permision-letter"
      loadingState={CL_loadingState}
      onSubmitF={(data: rawFormCredit): CreatePermisionLetterDto => {
        var activityCode = `${data.activityCode.value}`;
        var budgetCode = `${data.budgetCode.value}`;

        return {
          ...data,
          activityCode,
          budgetCode,
        };
      }}
      onSettledF={refreshGET && refreshGET}
    >
      <Heading>ثبت اطلاعات کلی نامه ایجاد تعهد</Heading>
      <Grid templateColumns={"repeat(2, 216px)"} gap={"12px"}>
        <PermisionLetterNoTextInput />
        <DateInput />
        <BudgetCodeInput />
        <ActivityCodeInput />
      </Grid>
      <Box mt={"28px"}>
        <SubmitButton isloading={CL_loadingState[0]}>ثبت نامه</SubmitButton>
      </Box>
    </Form>
  );
};
