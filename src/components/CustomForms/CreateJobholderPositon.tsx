import { useState } from "react";
import Form from "../Form";
import { Box, Flex, Heading } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import { PositionNameInput } from "../Inputs/position.inputs";

export const CreateJobholderPosition = ({
  log = false,
  refreshGET,
}: {
  log?: boolean;
  refreshGET?: () => void;
}) => {
  log && console.log("CreateJobholderPosition refreshGET: ", refreshGET);
  const loadingState = useState(false);
  return (
    <Form
      url="organisation/jobholder"
      loadingState={loadingState}
      onSettledF={refreshGET && refreshGET}
    >
      <Heading>موقعیت افسر خرید جدید</Heading>
      <Flex gap={"8px"} flexWrap={"wrap"}>
        <PositionNameInput />
        <SubmitButton
          isloading={loadingState[0]}
          w={"250px"}
          alignSelf={"flex-end"}
          size={"sm"}
        >
          اضافه کردن موقعیت افسر خرید
        </SubmitButton>
      </Flex>
    </Form>
  );
};
