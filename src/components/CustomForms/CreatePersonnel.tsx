import { useState, useEffect } from "react";
import Form from "../Form";
import { Box, Flex, Heading } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import {
  LastNameInput,
  NameInput,
  PersonnelIdInput,
  RankInput,
} from "../Inputs/personnel.inputs";

export const CreatePersonnel = ({
  log = false,
  refreshGET,
}: {
  log?: boolean;
  refreshGET?: () => void;
}) => {
  log && console.log("CreatePersonnel refreshGET: ", refreshGET);
  const loadingState = useState(false);
  return (
    <Form
      url="organisation/personnel"
      loadingState={loadingState}
      onSettledF={refreshGET && refreshGET}
    >
      <Heading>پرسنل جدید</Heading>
      <Flex
        gap={"8px"}
        flexWrap={"wrap"}
        flexDir={{ base: "column", md: "row" }}
      >
        <PersonnelIdInput />
        <NameInput />
        <LastNameInput />
        <RankInput />
      </Flex>
      <Box mt={"28px"}>
        <SubmitButton isloading={loadingState[0]} width={"150px"} size={"sm"}>
          اضافه کردن پرسنل
        </SubmitButton>
      </Box>
    </Form>
  );
};
