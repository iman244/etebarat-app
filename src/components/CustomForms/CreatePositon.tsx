import { useState } from "react";
import Form from "../Form";
import { Flex, Heading } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import { PositionNameInput } from "../Inputs/position.inputs";

export const CreatePosition = ({
  log = false,
  refreshGET,
}: {
  log?: boolean;
  refreshGET?: () => void;
}) => {
  log && console.log("CreatePosition refreshGET: ", refreshGET);
  const loadingState = useState(false);
  return (
    <Form
      url="organisation/position"
      loadingState={loadingState}
      onSettledF={refreshGET && refreshGET}
    >
      <Heading>موقعیت جدید</Heading>
      <Flex gap={"8px"} flexWrap={"wrap"}>
        <PositionNameInput />
        <SubmitButton
          isloading={loadingState[0]}
          w={"200px"}
          size={"sm"}
          alignSelf={"flex-end"}
        >
          اضافه کردن موقعیت
        </SubmitButton>
      </Flex>
    </Form>
  );
};
