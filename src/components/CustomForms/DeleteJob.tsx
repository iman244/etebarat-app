import React, { useState } from "react";
import { job } from "../../types/backendTypes/entities";
import { Flex } from "@chakra-ui/react";
import Form from "../Form";
import { SubmitButton } from "../FormComponents";
import { Field } from "../TextComponents";

export const DeleteJob = ({
  job,
  refreshGET,
  onClose,
  log = true,
}: {
  log?: boolean;
  job: job;
  refreshGET?: () => void;
  onClose?: () => void;
}) => {
  log && console.log("ModifyJob job", job);
  return (
    <Flex flexDir={"column"} alignItems={"center"} w={"100%"} dir="rtl">
      <JobInfo job={job} />

      <DeleteCreditBtn
        job={job}
        onSettledF={() => {
          refreshGET && refreshGET();
          onClose && onClose();
        }}
      />
    </Flex>
  );
};

const JobInfo = ({ job }: { job: job }) => {
  return (
    <Flex justifyContent={"space-between"} w={"100%"}>
      <Flex flexDir={"column"} gap={"8px"}>
        <Field field="ردیف بودجه" value={job.permisionLetter.budgetCode} />
        <Field field="شماره فعالیت" value={job.permisionLetter.activityCode} />
        <Field field="کد ملاحظات" value={`${job.molahezatCode}`} />
        <Field field="افسر خرید" value={job.personnel.lastName} />
      </Flex>
      <Flex flexDir={"column"} gap={"8px"}>
        <Field
          field="شماره نامه ایجاد تعهد"
          value={job.permisionLetter.letterNo}
        />
        <Field field="تاریخ" value={job.permisionLetter.date} />
      </Flex>
    </Flex>
  );
};

const DeleteCreditBtn = ({
  onSettledF,
  job,
}: {
  onSettledF?: () => void;
  job: job;
}) => {
  const loadingState = useState(false);

  return (
    <Form
      url={"credit/job"}
      method="delete"
      loadingState={loadingState}
      onSubmitF={() => ({ jobId: job.jobId })}
      onSettledF={onSettledF && onSettledF}
    >
      <SubmitButton variant="delete-btn" isloading={loadingState[0]}>
        حذف اعتبار
      </SubmitButton>
    </Form>
  );
};
