import { useState } from "react";
import Form from "../Form";
import {
  Box,
  Flex,
  Heading,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";

import {
  CreditInput,
  DescriptionInput,
  DetailedCodeInput,
  OperationCodeInput,
  SelectJobholderInput,
} from "../Inputs/Job.inputs";
import { SubmitButton } from "../FormComponents";
import { CreateJobFormData } from "../../types/frontTypes";
import { CreateJobDto } from "../../types/backendTypes/dtos";
import { permisionLetter } from "../../types/backendTypes/entities";

export const CreateJob = ({
  permisionLetter,
  log = true,
  refreshGET,
}: {
  permisionLetter: permisionLetter;
  log?: boolean;
  refreshGET?: () => void;
}) => {
  log && console.log("CreateJob permisionLetter", permisionLetter);
  const loadingState = useState(false);
  return (
    <Form
      url="/credit/job"
      loadingState={loadingState}
      onSubmitF={(data: CreateJobFormData): CreateJobDto => {
        console.log("submit data", data);
        return {
          permisionLetterId: permisionLetter.permisionLetterId,
          personnelId: Number(data.personnelId.value),
          creditValue: Number(data.creditValue),
          molahezatCode: Number(data.molahezatCode),
          description: data.description,
        };
      }}
      onSettledF={refreshGET && refreshGET}
      w={"100%"}
    >
      <Flex
        // w={"100%"}
        height={"fit-content"}
        // alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Flex flexDir={"column"} gap={"8px"}>
          <Box mt={"28px"}>
            <SelectJobholderInput />
          </Box>
          <DetailedCodeInput />
          <CreditInput />
        </Flex>
        <Flex alignSelf={"stretch"}>
          <DescriptionInput />
        </Flex>
      </Flex>
      <Box mt={"28px"}>
        <SubmitButton isloading={loadingState[0]}>ایجاد اعتبار</SubmitButton>
      </Box>
    </Form>
  );
};
