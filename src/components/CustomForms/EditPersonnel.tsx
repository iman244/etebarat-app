import { useState } from "react";
import Form from "../Form";
import { Divider, Flex, Heading, Text } from "@chakra-ui/react";
import { SubmitButton } from "../FormComponents";
import { jobholder, person, position } from "../../types/backendTypes/entities";
import { digitsFaToEn } from "@persian-tools/persian-tools";
import {
  LastNameInput,
  NameInput,
  RankInput,
} from "../Inputs/personnel.inputs";
import { CreatePersonnelDto } from "../../types/backendTypes/dtos";

export const EditPersonnel = ({
  log = true,
  refreshGET,
  onClose,
  person,
}: {
  log?: boolean;
  refreshGET?: () => void;
  onClose?: () => void;
  person: person;
}) => {
  log && console.log("EditPersonnel refreshGET: ", refreshGET);
  log && console.log("EditPersonnel person: ", person);
  const loadingState = useState(false);
  const loadingStatePersonnelDelete = useState(false);
  const loadingStatePositionDelete = useState(false);
  const loadingStateJobholderDelete = useState(false);
  return (
    <>
      <Flex justifyContent={"space-between"} alignItems={"center"}>
        <Heading>ویرایش پرسنل</Heading>
        <Form
          method="delete"
          url="organisation/personnel"
          loadingState={loadingStatePersonnelDelete}
          case200={() => {
            refreshGET && refreshGET();
            onClose && onClose();
          }}
          defaultValues={
            person && {
              ...person,
              personnelId: digitsFaToEn(`${person.personnelId}`),
            }
          }
        >
          <SubmitButton
            variant={"delete-btn"}
            isloading={loadingStatePersonnelDelete[0]}
            width={"100px"}
            size={"xs"}
          >
            حذف فرد
          </SubmitButton>
        </Form>
      </Flex>
      <Divider my={"8px"} />
      <Flex flexDir={"column"} gap={"4px"}>
        <Flex justifyContent={"space-between"} alignItems={"center"}>
          <Heading size={"sm"}>مسئولیت‌ها</Heading>
          {!person.position && !person.jobholder && (
            <Text fontSize={"smaller"}>این نیرو مسئولیتی در سازمان ندارد</Text>
          )}
        </Flex>
        {person.position && (
          <Form
            url={"organisation/position"}
            method="delete"
            loadingState={loadingStatePositionDelete}
            case200={refreshGET && refreshGET}
            onSubmitF={(): position => {
              return {
                positionName: person.position!.positionName,
              };
            }}
            w={"100%"}
          >
            <Flex justifyContent={"space-between"} alignItems={"center"}>
              <Text size={"xs"}>{person.position!.positionName}</Text>
              <SubmitButton
                variant={"delete-btn"}
                isloading={loadingStatePositionDelete[0]}
                width={"100px"}
                size={"xs"}
              >
                حذف مسئولیت
              </SubmitButton>
            </Flex>
          </Form>
        )}
        {person?.jobholder && (
          <Form
            url={"organisation/jobholder"}
            method="delete"
            loadingState={loadingStateJobholderDelete}
            case200={refreshGET && refreshGET}
            onSubmitF={(): jobholder => {
              return {
                positionName: person.jobholder!.positionName,
              };
            }}
            w={"100%"}
          >
            <Flex
              justifyContent={"space-between"}
              alignItems={"center"}
              w={"100%"}
            >
              <Text size={"xs"}>{person.jobholder!.positionName}</Text>
              <SubmitButton
                isloading={loadingStateJobholderDelete[0]}
                width={"100px"}
                bgColor={"red.100"}
                _hover={{ bgColor: "red.200" }}
                size={"xs"}
                spinnerColor="red.400"
              >
                حذف مسئولیت
              </SubmitButton>
            </Flex>
          </Form>
        )}
      </Flex>
      <Divider my={"16px"} />
      <Form
        method="patch"
        url="organisation/personnel"
        loadingState={loadingState}
        // edit and create personnel dto are same
        onSubmitF={(data): CreatePersonnelDto => {
          return {
            ...data,
            personnelId: digitsFaToEn(`${person.personnelId}`),
          };
        }}
        case200={() => {
          refreshGET && refreshGET();
          onClose && onClose();
        }}
        defaultValues={
          person && {
            ...person,
            personnelId: digitsFaToEn(`${person.personnelId}`),
          }
        }
      >
        <Flex gap={"8px"} flexWrap={"wrap"} justifyContent={"space-between"}>
          {[
            {
              text: "کد پرسنلی",
              component: (
                <Text fontWeight={"600"} w={"216px"}>
                  {person.personnelId}
                </Text>
              ),
            },
            { text: "نام", component: <NameInput /> },
            { text: "نام خانوادگی", component: <LastNameInput /> },
            { text: "درجه", component: <RankInput /> },
          ].map((item) => (
            <Flex
              key={item.text}
              gap={"8px"}
              justifyContent={"space-between"}
              w={"100%"}
            >
              <Text alignSelf={"flex-end"}>{item.text}</Text>
              {item.component}
            </Flex>
          ))}
        </Flex>
        <Flex gap={"12px"} mt={"28px"} justifyContent={"space-between"}>
          <SubmitButton isloading={loadingState[0]} width={"100%"}>
            ویرایش
          </SubmitButton>
        </Flex>
      </Form>
    </>
  );
};
