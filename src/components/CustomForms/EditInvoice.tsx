import React from "react";
import { DeleteBtn } from "../FormComponents";
import { invoice } from "../../types/backendTypes/entities";

export const EditInvoice = ({
  invoice,
  refreshGET,
  onClose,
}: {
  invoice: invoice;
  refreshGET?: () => void;
  onClose: () => void;
}) => {
  return (
    <DeleteBtn
      url={"credit/invoice"}
      onSubmitF={() => ({ invoiceId: invoice.invoiceId })}
      btnText="حذف فاکتور"
      onSettledF={() => {
        refreshGET && refreshGET();
        onClose();
      }}
    />
  );
};
