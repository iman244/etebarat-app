import { Button, Divider, Flex } from "@chakra-ui/react";
import { Link, Outlet, useLocation } from "react-router-dom";
import { RouteObject } from "react-router-dom";

export const PageLayouts = ({ pages }: { pages: RouteObject[] }) => {
  const { pathname } = useLocation();

  return (
    <Flex w={"100%"} h={"100%"} flexDir={"column"} gap={"24px"}>
      <Flex gap={"12px"}>
        {pages.map((RouteObject) => (
          <Button
            key={RouteObject.id}
            as={Link}
            to={RouteObject.path}
            variant={pathname === RouteObject.path ? "solid" : "outline"}
          >
            {RouteObject.id}
          </Button>
        ))}
      </Flex>
      <Divider />
      <Outlet />
    </Flex>
  );
};
