import { GridItem } from "@chakra-ui/react";
import React from "react";

export const GridHeader = ({ children }: any) => {
  return (
    <GridItem
      borderBottom="1px solid"
      padding="12px"
      borderColor="gray.300"
      w="100%"
      h={"100%"}
      textAlign="center"
      display={"flex"}
      flexDir={"column"}
      alignItems={"center"}
      justifyContent={"flex-end"}
    >
      {children}
    </GridItem>
  );
};
