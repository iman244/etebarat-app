import { Button, Flex, Spinner, Input, Text, Textarea } from "@chakra-ui/react";
import { Controller, useFormContext, useFormState } from "react-hook-form";
import {
  CompilicatedCreatableSelectProps,
  CompilicatedInputProps,
  CompilicatedSelectProps,
  SubmitButtonProps,
} from "../types/frontTypes";
import Select, { StylesConfig } from "react-select";
import { numberToWords } from "@persian-tools/persian-tools";
import CreatableSelect from "react-select/creatable";
import usePost from "../hooks/usePost";
import { useState } from "react";
import Form from "./Form";

export const ComplicatedInput = ({
  register,
  name,
  placeholder,
  rules,
  type = "text",
  w = "216px",
  ...rest
}: CompilicatedInputProps) => {
  const { control } = useFormContext();
  const { errors } = useFormState();
  return (
    <Flex flexDir={"column"} gap={"4px"} w={w}>
      <Text height={"24px"} fontSize={"0.8em"} color={"red.300"}>
        {errors[name] ? `${errors[name]?.message}` : ""}
      </Text>
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({ field: { value, onChange, ...field } }: any) => {
          if (type === "file") {
            return (
              <FormInputUI
                register={register}
                placeholder={placeholder}
                type={type}
                value={value?.fileName}
                onChange={(event: any) => {
                  onChange(event.target.files[0]);
                }}
                {...field}
                {...rest}
              />
            );
          }
          return (
            <FormInputUI
              register={register}
              placeholder={placeholder}
              type={type}
              value={value}
              onChange={onChange}
              {...field}
              {...rest}
            />
          );
        }}
      />
    </Flex>
  );
};

export const ComplicatedSelectInput = ({
  register,
  name,
  placeholder,
  options,
  rules,

  selectStyles,
  isClearable = true,
  defaultValue = null,
}: CompilicatedSelectProps) => {
  const { control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field }: any) => {
        return (
          <Select
            register={register}
            placeholder={placeholder}
            options={options}
            styles={selectStyles}
            isClearable={isClearable}
            {...field}
          />
        );
      }}
    />
  );
};

export const ComplicatedCreatableSelectInput = ({
  register,
  name,
  placeholder,
  rules,
  options,
  selectStyles,
  isClearable = true,
  isDisabled = false,
  onCreateOption,
  defaultValue = null,
}: CompilicatedCreatableSelectProps) => {
  const { control } = useFormContext();

  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field }: any) => {
        return (
          <CreatableSelect
            isDisabled={isDisabled}
            register={register}
            placeholder={placeholder}
            options={options}
            styles={selectStyles}
            isClearable={isClearable}
            onCreateOption={onCreateOption}
            {...field}
          />
        );
      }}
    />
  );
};

export const SubmitButton = ({
  children = "ارسال",
  variant,
  isloading,
  loadingTest = false,
  ...rest
}: SubmitButtonProps) => {
  let spinnerColor =
    variant === "delete-btn" ? "red.400" : "rgba(200,200,200,1)";
  return (
    <Button type="submit" variant={variant} {...rest} dir="rtl">
      <Flex
        alignItems={"center"}
        justifyContent={"center"}
        w={"fit-content"}
        textAlign={"center"}
        gap={"12px"}
      >
        {loadingTest ? (
          <Spinner color={spinnerColor} size="xs" />
        ) : (
          isloading && <Spinner color={spinnerColor} size="xs" />
        )}
        {children}
      </Flex>
    </Button>
  );
};

export const FormInputUI = ({ type, ...props }: any) => {
  if (type === "textarea") {
    return <Textarea {...props} autoComplete={"off"} />;
  } else {
    return <Input type={type} {...props} autoComplete={"off"} />;
  }
};

// export const NewLiabilityInputRow = () => {
//   return (
//     <>
//       <OperationCodeInput />
//       <DetailedCodeInput />
//       <DescriptionInput />
//       <CreditInput />
//     </>
//   );
// };

const selectStyles: StylesConfig<any> = {
  control: (styles) => ({ ...styles, width: "100%" }),
};

export const DeleteBtn = ({
  url,
  onSubmitF,
  onSettledF,
  btnText = "حذف",
}: {
  url: string;
  onSubmitF: () => void;
  onSettledF?: () => void;
  btnText?: string;
}) => {
  const loadingState = useState(false);

  return (
    <Form
      url={url}
      method="delete"
      loadingState={loadingState}
      onSubmitF={onSubmitF}
      onSettledF={onSettledF && onSettledF}
    >
      <SubmitButton variant="delete-btn" isloading={loadingState[0]}>
        {btnText}
      </SubmitButton>
    </Form>
  );
};
