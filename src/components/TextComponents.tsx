import { Flex, Text } from "@chakra-ui/react";
import React from "react";

export const Field = ({ field, value }: { field: string; value: string }) => {
  return (
    <Flex gap={"12px"}>
      <Text color={"gray.400"}>{field}:</Text>
      <Text>{value}</Text>
    </Flex>
  );
};
