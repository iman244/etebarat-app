import React from "react";
import { CircularProgress, Flex } from "@chakra-ui/react";
import { QueryStatus } from "react-query";
import { query } from "../types/frontTypes";

const QueryWindow = ({
  query,
  children,
  loadingTest = false,
  LoadingUI = () => (
    <Flex
      height={"100%"}
      justifyContent={"center"}
      alignItems={"center"}
      w={"100%"}
      flex={1}
    >
      <CircularProgress
        isIndeterminate
        capIsRound
        trackColor="transparent"
        thickness={"6px"}
        size={8}
        color="black"
      />
    </Flex>
  ),
  ErrorUI = ({ query }) => (
    <Flex
      padding={"24px"}
      bgColor={"red.100"}
      w={"fit-content"}
      h={"fit-content"}
      color={"red.500"}
    >
      {query!.error!.code}
    </Flex>
  ),
}: {
  query: query;
  loadingTest?: boolean;
  LoadingUI?: () => JSX.Element;
  ErrorUI?: ({ query }: { query: query }) => JSX.Element;
  children?: any;
}) => {
  const windowStatus = !loadingTest
    ? query
      ? query.status
      : "loading"
    : "loading";
  const window = (status: QueryStatus) => {
    if (query) {
      switch (status) {
        case "error":
          return <ErrorUI query={query} />;
        case "idle":
          return "idle";
        case "loading":
          return <LoadingUI />;
        case "success":
          return children;
        default:
          return "default";
      }
    } else return "query is not defined";
  };

  return <>{window(windowStatus)}</>;
};

export default QueryWindow;
