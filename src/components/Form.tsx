import { Box } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { FormProvider, useFormContext } from "react-hook-form";
import { FormLayoutProps, FormProps } from "../types/restTypes";
import usePost from "../hooks/usePost";
import { InternalFormLayout } from "./InternalFormLayout";
import { useEffect } from "react";

export default function Form({
  method = "post",
  loadingState,
  defaultValues,
  children,
  url,
  onSettledF,
  onSubmitF,
  case200,
  case201,
  case401,
  caseError,
  ...rest
}: FormProps) {
  const methods = useForm({ defaultValues });
  const { handleSubmit } = methods;
  const onSubmit = (data: any) => {
    loadingState && loadingState[1](true);
    var d = data;
    if (onSubmitF) {
      d = onSubmitF(data);
    }

    let form = new FormData();
    Object.entries(d).forEach(([field, value]: any, index) => {
      if (field === "file") {
        form.append(field, value);
      } else {
        form.append(field, value);
      }
    });

    POST.mutate(d);
  };
  const onError = (data: any) => console.log(data);

  const POST = usePost(
    url,
    loadingState,
    onSettledF,
    case200,
    case201,
    case401,
    caseError,
    method
  );

  return (
    <FormProvider {...methods}>
      <Box
        as="form"
        w={"fit-content"}
        onSubmit={handleSubmit(onSubmit, onError)}
        {...rest}
      >
        {children}
      </Box>
    </FormProvider>
  );
}

export const ConnectForm = ({ children }: any) => {
  const methods = useFormContext();

  return children({ ...methods });
};

export const FormLayout = ({ children, heading, ...rest }: FormLayoutProps) => {
  return (
    <Form {...rest}>
      <InternalFormLayout>{children}</InternalFormLayout>
    </Form>
  );
};
