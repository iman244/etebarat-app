import { RouteObject, createBrowserRouter } from "react-router-dom";
import { Layout } from "./routes/Layout";
import { IssueInvoice } from "./routes/invoice/issueInvoice/page";
import { PermisionLetters } from "./routes/credits/permisionLetters/page";
import { PrintTankhah } from "./routes/print-tankhah/page";
import { Personnel } from "./routes/organisation/personnel/page";
import { Positions } from "./routes/organisation/positions/page";
import { Jobholders } from "./routes/organisation/Jobholders/page";
import { PageLayouts } from "./components/PagesLayout";
import { Jobs } from "./routes/credits/jobs/page";
import { Invoices } from "./routes/invoice/listInvoices/page";

export const CreditsPages: RouteObject[] = [
  {
    id: "نامه‌های ایجاد تعهد",
    path: "/credits",
    element: <PermisionLetters />,
  },
  {
    id: "اعتبارات",
    path: "/credits/Jobs",
    element: <Jobs />,
  },
];

export const InvoicePages: RouteObject[] = [
  {
    id: "لیست فاکتورها",
    path: "/invoice",
    element: <Invoices />,
  },
  {
    id: "صادر/اصلاح کردن فاکتور",
    path: "/invoice/issueInvoice",
    element: <IssueInvoice />,
  },
];

export const OrganisaionsPages: RouteObject[] = [
  {
    id: "افراد سازمان",
    path: "/organisation",
    element: <Personnel />,
  },
  {
    id: "موقعیت‌های سازمان",
    path: "/organisation/positions",
    element: <Positions />,
  },
  {
    id: "افسران خرید",
    path: "/organisation/jobholders",
    element: <Jobholders />,
  },
];

export const Pages: RouteObject[] = [
  {
    id: "مدیریت اعتبار",
    path: "credits",
    element: <PageLayouts pages={CreditsPages} />,
    children: CreditsPages,
  },
  {
    id: "فاکتورها",
    path: "invoice",
    element: <PageLayouts pages={InvoicePages} />,
    children: InvoicePages,
  },
  {
    id: "چاپ تنخواه",
    path: "print-tankhah",
    element: <PrintTankhah />,
  },
  {
    id: "سازمان",
    path: "organisation",
    element: <PageLayouts pages={OrganisaionsPages} />,
    children: OrganisaionsPages,
  },
];

const RouteObjects: RouteObject[] = [
  {
    path: "/",
    element: <Layout />,
    children: Pages,
  },
];

const router = createBrowserRouter(RouteObjects);

export default router;
