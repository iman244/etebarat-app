import { person } from "./entities";

export interface error_Dtos {
  error: string;
  message: string[];
  statusCode: number;
}

export interface CreatePermisionLetterDto {
  letterNo: string;
  date: string;
  budgetCode: string;
  activityCode: string;
}

export interface CreatePersonnelDto {
  personnelId: number;
  name: string;
  lastName: string;
  rank: string;
}

export interface editPositionDto {
  positionName: string;
  personnelId?: number;
}

export interface CreateJobDto {
  permisionLetterId: number;
  personnelId: number;
  molahezatCode: number;
  description: string;
  creditValue: number;
}

export interface CreateJobholderDto {
  positionName: string;
  person?: person;
  jobs?: any;
}

export interface EditJobholderDto {
  positionName: string;
  person?: person;
  jobs?: any;
}

export interface purchaseDto {
  invoiceId: number;
  description: string;
  unit: string;
  quantity: number;
  unitPrice: number;
  purchaseValue: number;
}

export interface invoiceDto {
  invoiceId: string;
  jobId: number;
  date: string;
  vat: number;
  discount: number;
  for: string;
}

export interface issueInvoiceNumberDto {
  invoiceId: number;
}
