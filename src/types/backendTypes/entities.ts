export interface basePerson {
  personnelId: number;
  name: string;
  lastName: string;
  rank: string;
}

export interface basePosition {
  positionName: string;
}

export interface baseJobholder {
  positionName: string;
}

export interface basePermissionLetter {
  permisionLetterId: number;
  budgetCode: string;
  activityCode: string;
  date: string;
  letterNo: string;
}

export interface baseJob {
  jobId: number;
  molahezatCode: number;
  description: string;
  creditValue: number;
}

export interface person extends basePerson {
  position?: position;
  jobholder?: jobholder;
}

export interface position extends basePosition {
  person?: basePerson;
}

export interface jobholder extends baseJobholder {
  person?: basePerson;
  jobs?: any;
}

export interface permisionLetter extends basePermissionLetter {
  jobs: job[];
}

export interface job extends baseJob {
  permisionLetter: permisionLetter;
  personnel: person;
  invoices: invoice[];
}

export interface invoice {
  invoiceId: number;
  date: string;
  vat: number;
  discount: number;
  for: string;
  job: job;
  purchases: purchase[];
}

export interface purchase {
  purchaseId: string;
  invoice: invoice;
  description: string;
  unit: string;
  quantity: number;
  unitPrice: number;
  purchaseValue: number;
}
