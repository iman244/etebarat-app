import { ChakraProps, StyleProps } from "@chakra-ui/react";
import { Axios, AxiosRequestConfig, AxiosResponse, AxiosStatic } from "axios";
import { MutationFunction, UseMutationResult } from "react-query";

export interface FormProps extends usePostProps, StyleProps, ChakraProps {
  children?: any;
  defaultValues?: object;
  onSubmitF?: (data: any) => any;
}

export interface usePostProps {
  url: string;
  loadingState?: [boolean, React.Dispatch<React.SetStateAction<boolean>>];
  onSettledF?: POSTunknownThenFunctions;
  case200?: POSTsuccessThenFunctions;
  case201?: POSTsuccessThenFunctions;
  case401?: POSTsuccessThenFunctions;
  caseError?: POSTerrorThenFunctions;
  method?: string;
}

export type usePostHook = (
  url: string,
  loadingState?: [boolean, React.Dispatch<React.SetStateAction<boolean>>],
  onSettledF?: POSTunknownThenFunctions,
  case200?: POSTsuccessThenFunctions,
  case201?: POSTsuccessThenFunctions,
  case401?: POSTsuccessThenFunctions,
  caseError?: POSTerrorThenFunctions,
  method?: string
) => UseMutationResult<AxiosResponse<any, any>, unknown, any, unknown>;

export interface FormLayoutProps extends FormProps {
  children?: any;
  heading: string;
}

export type GETsuccessThenFunctions = ((data: any) => any) | undefined;
export type GETunknownThenFunctions =
  | ((data: any, error: unknown) => any)
  | undefined;
export type GETerrorThenFunctions = ((err: unknown) => any) | undefined;

export type POSTsuccessThenFunctions =
  | ((
      data: any,
      d: AxiosResponse<any, any>,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
export type POSTunknownThenFunctions =
  | ((
      data: AxiosResponse<any, any> | undefined,
      error: unknown,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
export type POSTerrorThenFunctions =
  | ((
      error: unknown,
      variables: void,
      context: unknown
    ) => void | Promise<unknown>)
  | undefined;
