import { ButtonProps, ChakraProps, StyleProps } from "@chakra-ui/react";
import { AxiosError, AxiosResponse } from "axios";
import { FieldValues, RegisterOptions } from "react-hook-form";
import { QueryStatus, UseQueryResult } from "react-query";
import { GroupBase, OptionsOrGroups, StylesConfig } from "react-select";

export interface simpleQuery<T> {
  data: T;
  status: QueryStatus;
}

export type query<T = any> =
  | UseQueryResult<AxiosResponse<T, any>, AxiosError>
  | undefined;

// export type QueryWindowStatus = QueryStatus | "not connected to container";

export interface CompilicatedInputProps extends ChakraProps {
  register: any;
  name: string;
  placeholder: string;
  rules:
    | Omit<
        RegisterOptions<FieldValues, any>,
        "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
      >
    | undefined;
  type?: React.HTMLInputTypeAttribute | "textarea";
}

export interface CompilicatedSelectProps extends ChakraProps {
  register: any;
  name: string;
  placeholder: string;
  rules:
    | Omit<
        RegisterOptions<FieldValues, any>,
        "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
      >
    | undefined;
  options: OptionsOrGroups<unknown, GroupBase<unknown>>;
  selectStyles?: StylesConfig<unknown, false, GroupBase<unknown>> | undefined;
  isClearable?: boolean;
  defaultValue?: any;
}

export interface CompilicatedCreatableSelectProps
  extends CompilicatedSelectProps {
  isDisabled?: boolean;
  onCreateOption: ((inputValue: string) => void) | undefined;
}

export type selectOptions = OptionsOrGroups<unknown, GroupBase<unknown>>;

export interface rawFormCredit {
  budgetCode: reactSelect<string, string>;
  activityCode: reactSelect<string, string>;
  date: string;
  letterNo: string;
}

export interface SubmitButtonProps
  extends ButtonProps,
    StyleProps,
    ChakraProps {
  children?: any;
  variant?: string;
  isloading: boolean;
  loadingTest?: boolean;
  spinnerColor?: string;
}

export interface CreateJobFormData {
  creditValue: string;
  description: string;
  molahezatCode: string;
  personnelId: reactSelect<string, number>;
}

export interface rawIssueInvoiceData {
  date: string;
  for: string;
  discount: number;
  invoiceId: reactSelect<string, number>;
  jobId: reactSelect<string, number>;
  vat: number;
}

export interface reactSelect<L = any, V = any> {
  label: string;
  value: number;
}
