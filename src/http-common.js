import axios from "axios";
export const backendIP = "http://localhost:8000";

export default axios.create({
  baseURL: `${backendIP}/`,
  // withCredentials: true,
  headers: {
    "Content-type": "application/json",
  },
});
