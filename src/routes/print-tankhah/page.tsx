import { Button, Flex, Text } from "@chakra-ui/react";
import React from "react";
import { Field } from "../../components/TextComponents";

export const PrintTankhah = () => {
  return (
    <Flex flexDir={"column"} w={"100%"}>
      <Button
        w={"fit-content"}
        sx={{
          "@media print": {
            display: "none",
          },
        }}
        onClick={() => {
          window.print();
        }}
      >
        چاپ تنخواه
      </Button>
      <TankhahGardanPage />
    </Flex>
  );
};

const TankhahGardanPage = () => {
  return (
    <Flex w={"100%"} flexDir={"column"} alignItems={"center"} gap={"12px"}>
      <Text>بسمه تعالی</Text>
      <LetterHeading />
      <Flex flexDir={"column"} w={"100%"}>
        <RequestHeading />
        <Govahi />
      </Flex>
    </Flex>
  );
};

const LetterHeading = () => {
  return (
    <Flex w={"100%"} justifyContent={"space-between"} alignItems={"center"}>
      <Text>تنخواه گردان</Text>
      <Flex flexDir={"column"} gap={"12px"}>
        <Flex gap={"8px"}>
          <Text>شماره:</Text>
          <Text>shomare</Text>
        </Flex>
        <Flex gap={"8px"}>
          <Text>تاریخ:</Text>
          <Text>tarikh</Text>
        </Flex>
      </Flex>
    </Flex>
  );
};

const RequestHeading = () => {
  return (
    <Flex w={"100%"}>
      <Flex
        flex={1}
        flexDir={"column"}
        padding={"8px"}
        border={"1px solid black"}
        borderLeft={"none"}
        gap={"12px"}
      >
        <Field field={"درخواست کننده"} value={"darkhast konande"} />
        <Flex justifyContent={"space-between"}>
          <Field field={"سرویس"} value={"service"} />
          <Text>مدیریت صاحب اعتبار</Text>
        </Flex>
      </Flex>
      <Flex
        flex={1}
        border={"1px solid black"}
        justifyContent={"center"}
        alignItems={"center"}
      >
        <Field field="شماره امریه" value={"shomare amriye"} />
      </Flex>
    </Flex>
  );
};

const Govahi = () => {
  return <Text>Govahi</Text>;
};
// 19*11
