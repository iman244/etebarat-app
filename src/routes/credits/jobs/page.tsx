import React from "react";
import { JobsPresenter } from "../../../presenters/Jobs.presenter";
import { JobsContainer } from "../../../containers/Jobs.container";

export const Jobs = () => {
  return (
    <JobsContainer>
      <JobsPresenter />
    </JobsContainer>
  );
};
