import { PermisionLettersContainer } from "../../../containers/PermisionLetters.container";
import { PermisionLettersPresenter } from "../../../presenters/PermisionLetters.presenter";

export const PermisionLetters = () => {
  return (
    <>
      <PermisionLettersContainer>
        <PermisionLettersPresenter />
      </PermisionLettersContainer>
    </>
  );
};
