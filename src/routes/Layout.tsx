import { Flex, Button } from "@chakra-ui/react";
import { Link, Outlet, useLocation } from "react-router-dom";
import { Pages } from "../router";

export const Layout = () => {
  return (
    <Flex dir="rtl" height={"100%"}>
      <Sidebar />
      <Content>
        <Outlet />
      </Content>
    </Flex>
  );
};

const Sidebar = () => {
  const { pathname } = useLocation();

  return (
    <Flex
      sx={{
        "@media print": {
          display: "none",
        },
      }}
      position={"fixed"}
      h={"100%"}
      w={"200px !important"}
      bgColor={"#f7f7f7"}
      borderLeft={"solid 1px #e3e3e3"}
      height={"100%"}
      padding={"12px"}
      boxSizing="border-box"
      flexDir={"column"}
      gap={"8px"}
    >
      <Button
        variant={pathname === "/" ? "sidebarLinkActive" : "sidebarLink"}
        as={Link}
        to="/"
      >
        صفحه اصلی
      </Button>
      {Pages.map((RouteObject) => {
        return (
          <Button
            key={RouteObject.id}
            variant={
              pathname === `/${RouteObject.path}`
                ? "sidebarLinkActive"
                : "sidebarLink"
            }
            as={Link}
            to={RouteObject.path}
          >
            {RouteObject.id}
          </Button>
        );
      })}
    </Flex>
  );
};

const Content = ({ children }: any) => {
  return (
    <Flex
      sx={{
        "@media print": {
          mr: "0px",
        },
      }}
      w={"100%"}
      flexDir={"column"}
      padding={"24px"}
      boxSizing="border-box"
      h={"fit-content"}
      mr={"200px"}
    >
      {children}
    </Flex>
  );
};
