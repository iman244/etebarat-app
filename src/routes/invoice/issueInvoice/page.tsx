import { Flex, Heading } from "@chakra-ui/react";
import React, { useState } from "react";
import {
  DiscountInput,
  ForInput,
  InvoiceDateInput,
  SelectInvoiceInput,
  SelectJobInput,
  VATInput,
} from "../../../components/Inputs/Invoice.inputs";
import Form from "../../../components/Form";
import { InvoicesContainer } from "../../../containers/Invoices.container";
import { InvoiceContainer } from "../../../containers/Invoice.container";
import { SubmitButton } from "../../../components/FormComponents";
import { invoiceDto } from "../../../types/backendTypes/dtos";
import { rawIssueInvoiceData } from "../../../types/frontTypes";

export const IssueInvoice = () => {
  const loadingState = useState(false);
  return (
    <Flex flexDir={"column"} gap={"8px"}>
      <Form
        url={"credit/invoice"}
        method="patch"
        loadingState={loadingState}
        onSubmitF={(data: rawIssueInvoiceData): invoiceDto => {
          return {
            ...data,
            invoiceId: `${data.invoiceId.value}`,
            jobId: data.jobId.value,
          };
        }}
      >
        <Flex flexDir={"column"} gap={"24px"}>
          <InvoicesContainer>
            <SelectInvoiceInput />
          </InvoicesContainer>
          <Flex justifyContent={"space-between"} gap={"48px"}>
            <Flex
              flexDir={"column"}
              gap={"12px"}
              paddingLeft={"48px"}
              borderLeft={"1px solid #E2E8F0"}
            >
              <Heading>اطلاعات خرید</Heading>
            </Flex>

            <Flex flexDir={"column"} gap={"12px"}>
              <Heading>اطلاعات کلی فاکتور</Heading>
              <InvoiceContainer>
                <SelectJobInput />
                <ForInput />
                <InvoiceDateInput />
                <VATInput />
                <DiscountInput />
              </InvoiceContainer>
              <SubmitButton isloading={loadingState[0]} w={"100px"} />
            </Flex>
          </Flex>
        </Flex>
      </Form>
    </Flex>
  );
};
