import React from "react";
import { InvoicesContainer } from "../../../containers/Invoices.container";
import { InvoicesPresenter } from "../../../presenters/Invoices.presenter";

export const Invoices = () => {
  return (
    <InvoicesContainer>
      <InvoicesPresenter />
    </InvoicesContainer>
  );
};
