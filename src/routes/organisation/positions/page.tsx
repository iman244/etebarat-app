import { PostionContainer } from "../../../containers/Position.container";
import { PositionPresenter } from "../../../presenters/Positon.presenter";

export const Positions = () => {
  return (
    <PostionContainer>
      <PositionPresenter />
    </PostionContainer>
  );
};
