import React, { useContext } from "react";
import { PersonnelContainer } from "../../../containers/Personnel.container";
import { PersonnelPresenter } from "../../../presenters/Personnel.presenter";
import { RefreshGETContext } from "../../../contexts/RefreshGETContextProvider";

export const Personnel = () => {
  return (
    <PersonnelContainer>
      <PersonnelPresenter />
    </PersonnelContainer>
  );
};
