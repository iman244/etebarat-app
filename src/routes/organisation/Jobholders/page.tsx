import React from "react";
import { JobholderContainer } from "../../../containers/Jobholder.container";
import { JobholderPresenter } from "../../../presenters/Jobholder.presenter";

export const Jobholders = () => {
  return (
    <JobholderContainer>
      <JobholderPresenter />
    </JobholderContainer>
  );
};
