import React, { Fragment, useState } from "react";
import { job } from "../types/backendTypes/entities";
import { query, simpleQuery } from "../types/frontTypes";
import {
  Flex,
  Heading,
  Modal,
  ModalContent,
  ModalOverlay,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import QueryWindow from "../components/QueryWindow";
import { addCommas, digitsEnToFa } from "@persian-tools/persian-tools";
import { DeleteJob } from "../components/CustomForms/DeleteJob";

export const JobsPresenter = ({
  log = false,
  query: jobsQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: query<job[]>;
  simpleQuery?: simpleQuery<job[]> | undefined;
  refreshGET?: () => void;
}) => {
  log && console.log("JobsPresenter query", jobsQuery);
  log && console.log("JobsPresenter simpleQuery", simpleQuery);
  log && console.log("JobsPresenter refreshGET", refreshGET);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [indexModalItem, setIndexModalItem] = useState(0);

  return (
    <Flex
      flexDir={"column"}
      gap={"15px"}
      bgColor={"gray.50"}
      padding={"15px"}
      minH={"200px"}
    >
      <Heading>لیست اعتبارات آماد</Heading>
      <QueryWindow query={jobsQuery}>
        <TableContainer>
          <Table variant={"striped"}>
            <Thead>
              <Tr>
                <Th>افسر خرید</Th>
                <Th>کد مشخصه</Th>
                <Th>شرح</Th>
                <Th>مبلغ</Th>
              </Tr>
            </Thead>
            <Tbody>
              {simpleQuery?.data.length === 0 && (
                <Text mt={"24px"}>اعتباری ایجاد نشده است</Text>
              )}
              {simpleQuery &&
                simpleQuery.data.map((job, index) => (
                  <Fragment key={job.jobId}>
                    <Tr
                      _hover={{
                        opacity: "0.5",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setIndexModalItem(index);
                        onOpen();
                      }}
                    >
                      <Td>
                        {job.personnel ? job.personnel.lastName : ".lastName"}
                      </Td>
                      <Td
                        w={"200px"}
                      >{`${job.permisionLetter.budgetCode}-${job.permisionLetter.activityCode}-${job.molahezatCode}`}</Td>
                      <Td>{job.description}</Td>
                      <Td>{digitsEnToFa(addCommas(job.creditValue))}</Td>
                    </Tr>
                  </Fragment>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
      </QueryWindow>
      {simpleQuery && (
        <ModalJob
          isOpen={isOpen}
          onClose={onClose}
          refreshGET={refreshGET}
          job={simpleQuery.data[indexModalItem]}
        />
      )}
    </Flex>
  );
};

const ModalJob = ({
  isOpen,
  onClose,
  job,
  refreshGET,
}: {
  isOpen: boolean;
  onClose: () => void;
  job: job;
  refreshGET?: () => void;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered size={"lg"}>
      <ModalOverlay />
      <ModalContent padding={"24px"}>
        <DeleteJob job={job} refreshGET={refreshGET} onClose={onClose} />
      </ModalContent>
    </Modal>
  );
};
