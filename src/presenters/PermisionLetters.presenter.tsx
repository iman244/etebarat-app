import React, { Fragment } from "react";
import { query, simpleQuery } from "../types/frontTypes";
import QueryWindow from "../components/QueryWindow";
import { UseQueryResult } from "react-query";
import { AxiosError, AxiosResponse } from "axios";
import {
  Divider,
  Flex,
  Heading,
  Modal,
  ModalContent,
  ModalOverlay,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { CreatePermisionLetter } from "../components/CustomForms/CreatePermisionLetter";
import { CreateJob } from "../components/CustomForms/CreateJob";
import { useState } from "react";
import { permisionLetter } from "../types/backendTypes/entities";

export const PermisionLettersPresenter = ({
  log = false,
  query: perLettersQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: query<permisionLetter[]>;
  simpleQuery?: simpleQuery<permisionLetter[]> | undefined;
  refreshGET?: () => void;
}) => {
  log && console.log("PermisionLettersPresenter query", perLettersQuery);
  log && console.log("PermisionLettersPresenter simpleQuery", simpleQuery);
  return (
    <Flex w={"100%"} h={"100%"} flexDir={"column"}>
      <Flex gap={"12px"}>
        <CreatePermisionLetter refreshGET={refreshGET} />
      </Flex>
      <Divider orientation="horizontal" my={"28px"} />
      <TablePermisionLetters
        perLettersQuery={perLettersQuery}
        simpleQuery={simpleQuery}
        refreshGET={refreshGET}
      />
    </Flex>
  );
};

const TablePermisionLetters = ({
  perLettersQuery,
  simpleQuery,
  refreshGET,
}: {
  perLettersQuery:
    | UseQueryResult<AxiosResponse<permisionLetter[], any>, AxiosError>
    | undefined;
  simpleQuery: simpleQuery<permisionLetter[]> | undefined;
  refreshGET?: () => void;
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [indexModalItem, setIndexModalItem] = useState(0);
  return (
    <Flex
      flexDir={"column"}
      gap={"15px"}
      bgColor={"gray.50"}
      padding={"15px"}
      minH={"200px"}
    >
      <Heading>لیست ایجاد تعهدات</Heading>
      <QueryWindow query={perLettersQuery}>
        <TableContainer>
          <Table variant={"striped"}>
            <Thead>
              <Tr>
                <Th>شماره فعالیت</Th>
                <Th>کد بودجه</Th>
                <Th>تاریخ</Th>
                <Th>شماره نامه</Th>
              </Tr>
            </Thead>
            <Tbody>
              {simpleQuery &&
                simpleQuery.data.map((permisionLetter, index) => (
                  <Fragment key={permisionLetter.permisionLetterId}>
                    <Tr
                      _hover={{
                        opacity: "0.5",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setIndexModalItem(index);
                        onOpen();
                      }}
                    >
                      <Td>{permisionLetter.activityCode}</Td>
                      <Td>{permisionLetter.budgetCode}</Td>
                      <Td>{permisionLetter.date}</Td>
                      <Td>{permisionLetter.letterNo}</Td>
                    </Tr>
                  </Fragment>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
      </QueryWindow>
      {simpleQuery && (
        <ModalCreateJob
          isOpen={isOpen}
          onClose={onClose}
          refreshGET={refreshGET}
          permisionLetter={simpleQuery.data[indexModalItem]}
        />
      )}
    </Flex>
  );
};

const ModalCreateJob = ({
  isOpen,
  onClose,
  permisionLetter,
  refreshGET,
}: {
  isOpen: boolean;
  onClose: () => void;
  refreshGET?: () => void;
  permisionLetter: permisionLetter;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered size={"xl"}>
      <ModalOverlay />
      <ModalContent padding={"24px"} dir="rtl">
        <Heading>ثبت ردیف های نامه ایجاد تعهد</Heading>

        <TableContainer>
          <Table variant={"striped"}>
            <Thead>
              <Tr>
                <Th>برنامه</Th>
                <Th>کد ملاحظات</Th>
                <Th>شرح</Th>
                <Th>مبلغ</Th>
              </Tr>
            </Thead>

            <Tbody>
              {permisionLetter &&
                permisionLetter.jobs.map((job) => {
                  return (
                    <Tr>
                      <Td>{job.personnel.jobholder?.positionName}</Td>
                      <Td>{job.molahezatCode}</Td>
                      <Td>{job.description}</Td>
                      <Td>{job.creditValue}</Td>
                    </Tr>
                  );
                })}
            </Tbody>
          </Table>
        </TableContainer>
        {permisionLetter && permisionLetter.jobs.length === 0 && (
          <Text fontSize={"xs"} mt={"12px"}>
            اعتباری برای این نامه ثبت نشده است
          </Text>
        )}
        <CreateJob permisionLetter={permisionLetter} refreshGET={refreshGET} />
      </ModalContent>
    </Modal>
  );
};
