import { query, simpleQuery } from "../types/frontTypes";
import QueryWindow from "../components/QueryWindow";
import { Divider, Flex, Heading, Text } from "@chakra-ui/react";
import { position } from "../types/backendTypes/entities";
import { EditPosition } from "../components/CustomForms/EditPosition";
import { CreatePosition } from "../components/CustomForms/CreatePositon";

export const PositionPresenter = ({
  log = false,
  query: positionQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: query<position[]>;
  simpleQuery?: simpleQuery<position[]> | undefined;
  refreshGET?: () => void;
}) => {
  log && console.log("PositionPresenter query", positionQuery);
  log && console.log("PositionPresenter simpleQuery", simpleQuery);

  return (
    <Flex flexDir={"column"} gap={"24px"}>
      <CreatePosition refreshGET={refreshGET} />
      <Divider />
      <Flex
        flexDir={"column"}
        gap={"15px"}
        bgColor={"gray.50"}
        padding={"15px"}
        minH={"200px"}
      >
        <Heading>لیست موقعیت‌های سازمان</Heading>
        <QueryWindow query={positionQuery}>
          {simpleQuery?.data.length === 0 && (
            <Text>موقعیتی در سازمان تعریف نشده است</Text>
          )}
          {simpleQuery?.data.map((position) => (
            <EditPosition
              key={position.positionName}
              url="organisation/position"
              position={position}
              refreshGET={refreshGET}
            />
          ))}
        </QueryWindow>
      </Flex>
    </Flex>
  );
};
