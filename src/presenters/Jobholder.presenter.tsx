import React from "react";
import { Divider, Flex, Heading, Text } from "@chakra-ui/react";
import QueryWindow from "../components/QueryWindow";
import { query, simpleQuery } from "../types/frontTypes";
import { jobholder } from "../types/backendTypes/entities";
import { EditPosition } from "../components/CustomForms/EditPosition";
import { CreateJobholderPosition } from "../components/CustomForms/CreateJobholderPositon";

export const JobholderPresenter = ({
  log = false,
  query: positionQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: query<jobholder[]>;
  simpleQuery?: simpleQuery<jobholder[]> | undefined;
  refreshGET?: () => void;
}) => {
  return (
    <Flex flexDir={"column"} gap={"24px"}>
      <CreateJobholderPosition refreshGET={refreshGET} />
      <Divider />
      <Flex
        flexDir={"column"}
        gap={"15px"}
        bgColor={"gray.50"}
        padding={"15px"}
        minH={"200px"}
      >
        <Heading>لیست افسران خرید سازمان</Heading>
        <QueryWindow query={positionQuery}>
          {simpleQuery?.data.length === 0 && (
            <Text>موقعیت افسر خریدی در سازمان تعریف نشده است</Text>
          )}

          {simpleQuery?.data.map((position) => (
            <EditPosition
              key={position.positionName}
              url="organisation/jobholder/appointment"
              deleteUrl="organisation/jobholder"
              position={position}
              refreshGET={refreshGET}
            />
          ))}
        </QueryWindow>
      </Flex>
    </Flex>
  );
};
