import { Fragment } from "react";
import { simpleQuery } from "../types/frontTypes";
import QueryWindow from "../components/QueryWindow";
import { UseQueryResult } from "react-query";
import { AxiosError, AxiosResponse } from "axios";
import {
  Divider,
  Flex,
  Heading,
  Modal,
  ModalContent,
  ModalOverlay,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { useState } from "react";
import { person } from "../types/backendTypes/entities";
import { CreatePersonnel } from "../components/CustomForms/CreatePersonnel";
import { EditPersonnel } from "../components/CustomForms/EditPersonnel";

export const PersonnelPresenter = ({
  log = false,
  query: personnelQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: UseQueryResult<AxiosResponse<person[], any>, AxiosError>;
  simpleQuery?: simpleQuery<person[]> | undefined;
  refreshGET?: () => void;
}) => {
  log && console.log("PersonnelPresenter query", personnelQuery);
  log && console.log("PersonnelPresenter simpleQuery", simpleQuery);
  log && console.log("PersonnelPresenter refreshGET", refreshGET);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [indexModalItem, setIndexModalItem] = useState(0);
  return (
    <Flex flexDir={"column"} gap={"24px"}>
      <CreatePersonnel refreshGET={refreshGET} />
      <Divider />
      <Flex
        flexDir={"column"}
        gap={"15px"}
        bgColor={"gray.50"}
        padding={"15px"}
        minH={"200px"}
      >
        <Heading>لیست افراد سازمان</Heading>
        <QueryWindow query={personnelQuery}>
          <TableContainer>
            <Table variant={"striped"}>
              <Thead>
                <Tr>
                  <Th>شماره پرسنلی</Th>
                  <Th>نام</Th>
                  <Th>نام خانوادگی</Th>
                  <Th>درجه</Th>
                </Tr>
              </Thead>
              <Tbody>
                {simpleQuery?.data.length === 0 && (
                  <Text>فردی در سازمان ایجاد نشده است</Text>
                )}

                {simpleQuery &&
                  simpleQuery.data.map((person, index) => (
                    <Fragment key={person.personnelId}>
                      <Tr
                        _hover={{
                          opacity: "0.5",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          setIndexModalItem(index);
                          onOpen();
                        }}
                      >
                        <Td>{person.personnelId}</Td>
                        <Td>{person.name}</Td>
                        <Td>{person.lastName}</Td>
                        <Td>{person.rank}</Td>
                      </Tr>
                    </Fragment>
                  ))}
              </Tbody>
            </Table>
          </TableContainer>
        </QueryWindow>
        {simpleQuery && (
          <ModalAppointment
            isOpen={isOpen}
            onClose={onClose}
            person={simpleQuery.data[indexModalItem]}
            refreshGET={refreshGET}
          />
        )}
      </Flex>
    </Flex>
  );
};

const ModalAppointment = ({
  isOpen,
  onClose,
  person,
  refreshGET,
}: {
  isOpen: boolean;
  onClose: () => void;
  person: person;
  refreshGET?: () => void;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered size={"sm"}>
      <ModalOverlay />
      <ModalContent padding={"24px"} dir="rtl">
        <EditPersonnel
          person={person}
          refreshGET={refreshGET && refreshGET}
          onClose={onClose}
        />
      </ModalContent>
    </Modal>
  );
};
