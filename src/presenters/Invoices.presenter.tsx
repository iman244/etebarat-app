import React, { Fragment, useState } from "react";
import { query, simpleQuery } from "../types/frontTypes";
import { invoice } from "../types/backendTypes/entities";
import {
  Flex,
  Heading,
  Modal,
  ModalContent,
  ModalOverlay,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import QueryWindow from "../components/QueryWindow";
import { EditInvoice } from "../components/CustomForms/EditInvoice";

export const InvoicesPresenter = ({
  log = false,
  query: invoicesQuery,
  simpleQuery,
  refreshGET,
}: {
  log?: boolean;
  query?: query<invoice[]>;
  simpleQuery?: simpleQuery<invoice[]> | undefined;
  refreshGET?: () => void;
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [indexModalItem, setIndexModalItem] = useState(0);

  log && console.log("InvoicesPresenter invoicesQuery", invoicesQuery);
  log && console.log("InvoicesPresenter simpleQuery", simpleQuery);
  return (
    <Flex
      flexDir={"column"}
      gap={"15px"}
      bgColor={"gray.50"}
      padding={"15px"}
      minH={"200px"}
    >
      <Heading>لیست فاکتورها</Heading>
      <QueryWindow query={invoicesQuery}>
        <TableContainer>
          <Table variant={"striped"}>
            <Thead>
              <Tr>
                <Th>شماره فاکتور</Th>
                <Th>شرح اعتبار</Th>
                <Th>کد مشخصه</Th>
                <Th>مبلغ</Th>
              </Tr>
            </Thead>
            <Tbody fontSize={"0.7rem"}>
              {simpleQuery?.data.length === 0 && (
                <Text mt={"24px"}>فاکتوری صادر نشده است</Text>
              )}
              {simpleQuery &&
                simpleQuery.data.map((invoice, index) => (
                  <Fragment key={invoice.invoiceId}>
                    <Tr
                      _hover={{
                        opacity: "0.5",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setIndexModalItem(index);
                        onOpen();
                      }}
                    >
                      <Td>{invoice.invoiceId}</Td>
                      <Td>
                        {invoice.job
                          ? invoice.job.description
                          : "محل اعتبار مشخص نشده است"}
                      </Td>
                      <Td w={"200px"}>
                        {invoice.job
                          ? ` ${invoice.job.permisionLetter.budgetCode}-${invoice.job.permisionLetter.activityCode}-${invoice.job.molahezatCode}`
                          : "محل اعتبار مشخص نشده است"}
                      </Td>
                      <Td>0</Td>
                      {/* <Td>{digitsEnToFa(addCommas(job.creditValue))}</Td> */}
                    </Tr>
                  </Fragment>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
      </QueryWindow>
      {simpleQuery && (
        <ModalInvoice
          invoice={simpleQuery.data[indexModalItem]}
          isOpen={isOpen}
          onClose={onClose}
          refreshGET={refreshGET}
        />
      )}
    </Flex>
  );
};

const ModalInvoice = ({
  isOpen,
  onClose,
  invoice,
  refreshGET,
}: {
  isOpen: boolean;
  onClose: () => void;
  invoice: invoice;
  refreshGET?: () => void;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered size={"lg"}>
      <ModalOverlay />
      <ModalContent padding={"24px"}>
        <EditInvoice
          invoice={invoice}
          refreshGET={refreshGET}
          onClose={onClose}
        />
      </ModalContent>
    </Modal>
  );
};
