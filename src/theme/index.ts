import { extendTheme } from "@chakra-ui/react";
import Button from "./components/Button";
import GridItem from "./components/GridItem";
import Text from "./components/Text";
import Heading from "./components/Heading";
import Td from "./components/Td";

const overrides = {
  components: {
    Td,
    Text,
    Heading,
    GridItem,
    Button,
  },
};
export default extendTheme(overrides);
