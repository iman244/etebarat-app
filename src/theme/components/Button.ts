import { defineStyleConfig, theme } from "@chakra-ui/react";

const Button = defineStyleConfig({
  variants: {
    sidebarLink: (props) => ({
      ...theme.components.Button.variants?.ghost(props),
      justifyContent: "flex-start",
      fontWeight: "normal",
    }),
    sidebarLinkActive: (props) => ({
      ...theme.components.Button.variants?.solid(props),
      bgColor: "gray.200",
      justifyContent: "flex-start",
    }),
    "delete-btn": {
      bgColor: "red.100",
      _hover: { bgColor: "red.200" },
    },
  },
});

export default Button;
//  bgColor={"red.100"}
//  _hover={{ bgColor: "red.200" }}
