import { defineStyleConfig } from "@chakra-ui/react";

const GridItem = defineStyleConfig({
  variants: {
    gridHeader: (props) => ({
      borderBottom: "1px solid",
      padding: "12px",
      borderColor: "gray.300",
      w: "100%",
      textAlign: "center",
    }),
  },
});

export default GridItem;
