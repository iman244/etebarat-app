import { defineStyleConfig } from "@chakra-ui/react";

const Heading = defineStyleConfig({
  baseStyle: {
    marginY: "12px",
  },
  defaultProps: {
    size: "md",
  },
});

export default Heading;
