import React, { createContext, useCallback, useMemo, useReducer } from "react";

export const RefreshGETContext = createContext({
  ghostRefreshToken1: 0,
  refreshGET1: () => {
    console.log("2");
  },
  ghostRefreshToken2: 0,
  refreshGET2: () => {
    console.log("2");
  },
});

function reducer1(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

function reducer2(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

export const RefreshGETContextProvider = ({ children }: any) => {
  const [ghostRefreshToken1, dispatch1] = useReducer(reducer1, 1);
  const [ghostRefreshToken2, dispatch2] = useReducer(reducer2, 1);
  const refreshGET1 = useCallback(() => dispatch1({ type: "refresh" }), []);
  const refreshGET2 = useCallback(() => dispatch2({ type: "refresh" }), []);
  const contextValue = useMemo(
    () => ({
      ghostRefreshToken1,
      refreshGET1: () => dispatch1({ type: "refresh" }),
      ghostRefreshToken2,
      refreshGET2: () => dispatch2({ type: "refresh" }),
    }),
    [ghostRefreshToken1, refreshGET1, ghostRefreshToken2, refreshGET2]
  );
  return (
    <RefreshGETContext.Provider value={contextValue}>
      {children}
    </RefreshGETContext.Provider>
  );
};
