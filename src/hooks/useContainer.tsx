import React, { useEffect, useReducer, useState } from "react";
import { simpleQuery } from "../types/frontTypes";
import useGet from "./useGet";
import { useNavigate } from "react-router-dom";
import { UseQueryResult } from "react-query";
import { AxiosResponse } from "axios";

interface options<D, E> {
  enabled?: boolean;
  log?: boolean;
  keys: any[];
  defaultValue?: D;
  case200?: (data: any) => D;
  case401?: (data: any) => E;
  case404?: (data: any) => void;
}

function reducer(state: number, action: { type: "refresh" }) {
  switch (action.type) {
    case "refresh":
      return state + 1;
  }
}

// type container<D, E> = (url: string, options: options<D, E>) => query<D>;

export function useContainer<D, E = any>(
  url: string,
  options: options<D, E>
): {
  simpleQuery: simpleQuery<D | E | undefined>;
  Query: UseQueryResult<AxiosResponse<any, any>, unknown>;
  refreshGET: () => void;
} {
  const [refreshToken, dispatch] = useReducer(reducer, 0);

  const refreshGET = () => dispatch({ type: "refresh" });

  const router = useNavigate();
  const [dataQuery, setDataQuery] = useState<simpleQuery<D | E | undefined>>({
    data: options.defaultValue,
    status: "loading",
  });

  options.log &&
    console.log(
      options.keys,
      "enabled",
      options.enabled !== undefined ? options.enabled : true
    );
  const [data, setData] = useState<D | E | undefined>(options.defaultValue);
  const GET = useGet(url, [...options.keys, refreshToken], {
    enabled: options.enabled !== undefined ? options.enabled : true,
    log: options.log,
    case200: (data) => {
      options.log &&
        console.log(`useContainer ${options.keys} default case200`, data);
      if (options.case200) {
        const pData = options.case200(data);
        setData(pData);
      } else {
        setData(data);
      }
    },
    case401: (data) => {
      if (options.case401) {
        const pData = options.case401(data);
        setData(pData);
      } else {
        router("/authenticate/sign-in");
      }
    },
    case404: options.case404 && options.case404,
  });

  useEffect(() => {
    options.log && console.log("useCotainer GET:", GET);
    setDataQuery({ data, status: GET.status });
  }, [data, GET.status]);

  return { simpleQuery: dataQuery, Query: GET, refreshGET };
}

export function ChildrenWithProps(
  children: any,
  simpleQuery: any,
  query: any,
  refreshGET: () => void
) {
  return React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement<any>(child, { query, simpleQuery, refreshGET });
    }
    return child;
  });
}
